const modal = getElement('#modal');
const modalShowBtn = getElement('#show-modal-btn');
const modalCloseBtn = getElement('#close-modal-btn');
const bookmarkForm = getElement('#bookmark-form');
const websiteName = getElement('#website-name');
const websiteUrl = getElement('#website-url');
const bookmarksContainer = getElement('#bookmarks-container');

let bookmarks = {};

function getElement(selector) {
	return document.querySelector(selector);
}

// Show modal, focus on input
function showModal() {
	modal.classList.add('show-modal');
	websiteName.focus();
}

// Validate form
function validate(name, url) {
	const expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/g;
	const regex = new RegExp(expression);

	// Make sure inputs fields are populated
	if (!name || !url) {
		alert('Please submit values for both fields.');
		return false;
	}

	// validate url
	if (!url.match(regex)) {
		alert('Please provide a valid web address');
		return false;
	}

	// Valid inputs
	return true;
}

// Build Bookmarks DOM
function buildBookmarks() {
	// Remove all bookmark elemets
	bookmarksContainer.textContent = '';
	// Build items
	Object.keys(bookmarks).forEach((id) => {
		const { name, url } = bookmarks[id];
		// Item elm
		const item = document.createElement('div');
		item.classList.add('item');
		// Close icon
		const closeIcon = document.createElement('i');
		closeIcon.classList.add('fas', 'fa-times');
		closeIcon.setAttribute('title', 'Delete bookmark');
		closeIcon.setAttribute('onclick', `deleteBookmark('${id}')`);
		// Favicon / Link container
		const linkInfo = document.createElement('div');
		linkInfo.classList.add('name');
		// Favicon
		const favicon = document.createElement('img');
		favicon.setAttribute(
			'src',
			`https://www.google.com/s2/favicons?domain=${url}`
		);
		favicon.setAttribute('alt', 'Favicon');
		// Link
		const link = document.createElement('a');
		link.setAttribute('href', `${url}`);
		link.setAttribute('target', '_blank');
		link.textContent = name;
		// Append to bookmarks container
		linkInfo.append(favicon, link);
		item.append(closeIcon, linkInfo);
		bookmarksContainer.appendChild(item);
	});
}

// Fetch bookmarks from localStorage
function fetchBookmarks() {
	if (localStorage.getItem('bookmarks')) {
		bookmarks = JSON.parse(localStorage.getItem('bookmarks'));
	}

	buildBookmarks();
}

// Delete bookmark
function deleteBookmark(id) {
	if (bookmarks[id]) {
		delete bookmarks[id];
	}

	// Update bookmarks array in localStorage, re-populate DOM
	localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
	fetchBookmarks();
}

// Handle data from form
function storeBookmark(e) {
	e.preventDefault();

	const name = websiteName.value;
	let url = websiteUrl.value;
	if (!url.includes('http://', 'https://')) {
		url = `https://${url}`;
	}

	if (!validate(name, url)) {
		return;
	}

	const bookmark = {
		name,
		url,
	};

	bookmarks[name] = bookmark;
	localStorage.setItem('bookmarks', JSON.stringify(bookmarks));
	fetchBookmarks();
	bookmarkForm.reset();
	// websiteName.focus();
	modal.classList.remove('show-modal');
}

// Event listeners
modalShowBtn.addEventListener('click', showModal);
modalCloseBtn.addEventListener('click', () =>
	modal.classList.remove('show-modal')
);
window.addEventListener('click', ({ target }) =>
	target === modal ? modal.classList.remove('show-modal') : null
);
bookmarkForm.addEventListener('submit', storeBookmark);

// On load
fetchBookmarks();
